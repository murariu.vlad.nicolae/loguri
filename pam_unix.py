from selectare_continut import *
from creare_output import *
from cron import *
import re


def verificare_pam_unix(continut_pam_unix, total_useri):
    #se verifica daca root si www-data sunt cele asteptate din cron si se paseaza mai departe orice este in plus
    if verificare_total_useri(total_useri, continut_pam_unix):
        de_sters = re.search("(cron:.*?\n\n)", continut_pam_unix, flags = re.DOTALL).group(1)
        continut_pam_unix = continut_pam_unix.replace(de_sters, "")
    rezultat = continut_pam_unix.strip()
    rezultat = creare_output_wiki(rezultat)
    return rezultat


def verificare_total_useri(total_useri, continut_pam_unix):
    useri_ok = True
    for user, total in total_useri.items():
        nume_user = user.replace("user ", "")
        if int(re.search(f"cron:\n.*?{nume_user}: (\d+).*?", continut_pam_unix, flags = re.DOTALL).group(1)) != total:
            useri_ok = False
    return useri_ok
