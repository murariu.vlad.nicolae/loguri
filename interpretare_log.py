from selectare_continut import *
from cron import *
from httpd import *
from pam_unix import *
from disk_space import *
from named import *
from smartd import *
from zfs import *
from templates import *


def interpretare_log(log):
    
    host = identificare_host(log)
    log = formatare_log(log)
    raport = template_raport_wiki(host)
    
    if cron in raport and f"{cron} Begin" in log:
       cron_total = section(log, cron)
       cron_continut = cron_total["continut sectiune"]
       cron_rezultat = verificare_cron(cron_continut, host)
       log = cron_total["log ramas"]
       total_useri = cron_rezultat["total useri"]
       raport[cron] = cron_rezultat["rezultat"]  

    if httpd in raport and f"{httpd} Begin" in log:
       httpd_total = section(log, httpd)
       httpd_continut = httpd_total["continut sectiune"]
       raport[httpd] = verificare_httpd(httpd_continut, host)
       log = httpd_total["log ramas"]
 
    if pam_unix in raport and f"{pam_unix} Begin" in log:
       pam_unix_total = section(log, pam_unix)
       pam_unix_continut = pam_unix_total["continut sectiune"]
       raport[pam_unix] = verificare_pam_unix(pam_unix_continut, total_useri)
       log = pam_unix_total["log ramas"]

    if named in raport and f"{named} Begin" in log:
       named_total = section(log, named)
       named_continut = named_total["continut sectiune"]
       raport[named] = verificare_named(named_continut, host)
       log = named_total["log ramas"]

    if disk_space in raport and f"{disk_space} Begin" in log:
       disk_space_total = section(log, disk_space)
       disk_space_continut = disk_space_total["continut sectiune"]
       raport[disk_space] = verificare_disk_space(disk_space_continut, host)
       log = disk_space_total["log ramas"]

    if zfs_report in raport and f"{zfs_report} Begin" in log:
        zfs_report_total = section(log, zfs_report)
        zfs_continut = zfs_report_total["continut sectiune"]
        raport[zfs_report] = verificare_zfs(zfs_continut)
        log = zfs_report_total["log ramas"]
        
    if smartd in raport and f"{smartd} Begin" in log:
        smartd_total = section(log, smartd)
        smartd_continut = smartd_total["continut sectiune"]
        raport[smartd] = verificare_smartd(smartd_continut, host)
        log = smartd_total["log ramas"]
       
    if kernel in raport and f"{kernel} Begin" in log:
        kernel_total = section(log, kernel)
        raport[kernel] = kernel_total["continut sectiune"].strip()
        log = kernel_total["log ramas"]
 
    if dpkg in raport and f"{dpkg} Begin" in log:
        dpkg_total = section(log, dpkg)
        raport[dpkg] = dpkg_total["continut sectiune"].strip()
        log = dpkg_total["log ramas"]         

    if connections in raport and f"{connections} Begin" in log:
        connections_total = section(log, connections_regex)
        raport[connections] = connections_total["continut sectiune"].strip()
        log = connections_total["log ramas"] 

    if sshd in raport and f"{sshd} Begin" in log:
        sshd_total = section(log, sshd)
        raport[sshd] = sshd_total["continut sectiune"].strip()
        log = sshd_total["log ramas"]
      
    if sudo in raport and f"{sudo} Begin" in log:
        sudo_total = section(log, sudo_regex)
        raport[sudo] = sudo_total["continut sectiune"].strip()
        log = sudo_total["log ramas"]        
 
    if rsyslogd in raport and f"{rsyslogd} Begin" in log:
        rsyslogd_total = section(log, rsyslogd)
        raport[rsyslogd] = rsyslogd_total["continut sectiune"].strip()
        log = rsyslogd_total["log ramas"]  

    if kernel_audit in raport and f"{kernel_audit} Begin" in log:
        kernel_audit_total = section(log, kernel_audit)
        raport[kernel_audit] = kernel_audit_total["continut sectiune"].strip()
        log = kernel_audit_total["log ramas"] 

    if dhcp_logs in raport and f"{dhcp_logs} Begin" in log:
        dhcp_logs_total = section(log, dhcp_logs)
        raport[dhcp_logs] = dhcp_logs_total["continut sectiune"].strip()
        log = dhcp_logs_total["log ramas"] 
        
    if fail2ban in raport and f"{fail2ban} Begin" in log:
        fail2ban_total = section(log, fail2ban)
        raport[fail2ban] = fail2ban_total["continut sectiune"].strip()
        log = fail2ban_total["log ramas"]          
        
    if iptables_firewall in raport and f"{iptables_firewall} Begin" in log:
        iptables_firewall_total = section(log, smartd)
        raport[iptables_firewall] = iptables_firewall_total["continut sectiune"].strip()
        log = iptables_firewall_total["log ramas"] 
      
    log = log.strip()
    if log != "":
        raport["neidentificate"] = log
        
    return {"host" : host, "raport wiki" : generare_raport_wiki(raport), "raport monitorizare" : generare_raport_monitorizare(raport)}

