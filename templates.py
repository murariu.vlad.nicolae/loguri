#Aici avem template-ul pentru raportul ce se trimite pe mail

template_raport_monitorizare = "pve1/pve2/pve4/pve5/pve6/pve7/pve8/speedtest/cacti2/fwbackbone/dhcp01/dns1/dns2/nagios/VM 105/VM 107/VM 120/VM 125/VM 126/VM 134/VM 240/VM 244/ns/ns2/bastion/erp/halnyftp/halnydhcp"

#Avem lista cu numele logurilor care trebuie verificate

loguri_lungi = ["erp", "pve5", "pve1", "pve6", "pve2", "pve8", "pve7", "halnyftp", "halnydhcp", "ns", "ns2", "dns1", "dns2", "speedtest", "fwbackbone", "bastion", "nagios", "pve4", "dhcp01", "cacti2"]

backups = ["pve8.jcs.local", "pve7.jcs.local", "pve5.jcs.local"]

#Avem modelul pentru subiectul unui mail

template_subiect_mail_log = "Logwatch for {} (Linux)"

#Functia va forma subiectul mailului pentru loguri

def formeaza_subiect_mail_log(nume_log):
    return template_subiect_mail_log.format(nume_log)

#Aici salvam stringurile dupa care se vor cauta sectiunile din log

cron = "Cron"

httpd = "httpd"

pam_unix = "pam_unix"

sshd = "SSHD"

sudo = "Sudo (secure-log)"

sudo_regex = "Sudo \(secure-log\)"

disk_space = "Disk Space"

kernel = "Kernel"

kernel_audit = "Kernel Audit"

dpkg = "dpkg status changes"

named = "Named"

rsyslogd = "rsyslogd"

connections = "Connections (secure-log)"

connections_regex = "Connections \(secure-log\)"

dhcp_logs = "DHCP logs"

fail2ban = "fail2ban-messages"    

smartd = "Smartd"  

iptables_firewall = "iptables firewall"

zfs_report = "ZFS Report"

zfs_pool_summary = "ZFS Pool Summary"

zfs_pool_status = "ZFS Pool Status"

#Aici tinem conditiile pentru partea de inceput din ZFS Report, cea cu totalurile

zfs_totaluri_asteptate = {}

zfs_totaluri_asteptate["zfs pools"] = {"Total ZFS pools: +(\d+)" : "1"}

zfs_totaluri_asteptate["filesystems"] = {"Total filesystems: +(\d+) \(" : "4"}

zfs_totaluri_asteptate["filesystems mounted"] = {"Total filesystems:.*\((\d+) mounted\)" : "4"}

zfs_totaluri_asteptate["snapshots"] = {"Total snapshots: +(\d+)" : "0"}

zfs_totaluri_asteptate["volumes"] = {"Total volumes: +(\d+)" : "3"}
 
#Aici tinem conditiile pentru partea de ZFS Pool Status   
    
zfs_pool_status_conditii = {}

zfs_pool_status_conditii["general"] = {"state: (\w+)" : "online"}

zfs_pool_status_conditii["rpool"] = {"rpool\s+(\w+)\s+\d" : "online"}

zfs_pool_status_conditii["mirror"] = {"mirror-0\s+(\w+)\s+\d" : "online"}

zfs_pool_status_conditii["ata1"] = {"ata.*WMC5C0D6AE0Y-part3\s+(\w+)\s+\d" : "online"}

zfs_pool_status_conditii["ata2"] = {"ata.*WMC5C0D73JLD-part3\s+(\w+)\s+\d" : "online"}

zfs_pool_status_conditii["errors"] = {"errors: (.*?)\n" : "no known data errors"}

#Aici vom tine template-urile pentru raportul din wiki pentru fiecare host

template_wiki = {}

template_wiki[""] = ""

template_wiki["cacti2"]  = f"{cron}/{httpd}/{pam_unix}/{sshd}/{sudo}/{disk_space}/{kernel}"

template_wiki["pve1"]  = f"{cron}/{iptables_firewall}/{httpd}/{dpkg}/{named}/{kernel}/{pam_unix}/{sshd}/{sudo}/{disk_space}"

template_wiki["pve2"]  = f"{cron}/{httpd}/{kernel}/{pam_unix}/{smartd}/{disk_space}/{zfs_report}"

template_wiki["pve4"]  = f"{cron}/{httpd}/{dpkg}/{named}/{kernel}/{pam_unix}/{sshd}/{sudo}/{disk_space}"

template_wiki["pve5"]  = f"{cron}/{httpd}/{dpkg}/{named}/{kernel}/{pam_unix}/{sshd}/{sudo}/{disk_space}"

template_wiki["pve6"]  = f"{cron}/{httpd}/{dpkg}/{named}/{kernel}/{pam_unix}/{connections}/{sshd}/{kernel_audit}/{sudo}/{disk_space}"

template_wiki["pve7"]  = f"{kernel_audit}/{dpkg}/{cron}/{kernel}/{httpd}/{pam_unix}/{connections}/{smartd}/{sshd}/{disk_space}"

template_wiki["pve8"]  = f"{kernel_audit}/{dpkg}/{cron}/{kernel}/{httpd}/{pam_unix}/{connections}/{smartd}/{sshd}/{disk_space}"

template_wiki["speedtest"]  = f"{cron}/{httpd}/{dpkg}/{named}/{kernel}/{pam_unix}/{sshd}/{sudo}/{connections}/{disk_space}"

template_wiki["fwbackbone"]  = f"{cron}/{kernel_audit}/{pam_unix}/{httpd}/{dpkg}/{named}/{kernel}/{rsyslogd}/{connections}/{sshd}/{sudo}/{disk_space}"

template_wiki["dhcp01"]  = f"{cron}/{dhcp_logs}/{fail2ban}/{httpd}/{dpkg}/{named}/{kernel}/{pam_unix}/{sshd}/{sudo}/{disk_space}"

template_wiki["dns1"]  = f"{cron}/{dpkg}/{kernel}/{named}/{pam_unix}/{rsyslogd}/{connections}/{sshd}/{sudo}/{disk_space}"

template_wiki["dns2"]  = f"{cron}/{dpkg}/{kernel}/{named}/{pam_unix}/{rsyslogd}/{connections}/{sshd}/{sudo}/{disk_space}"

template_wiki["ns"]  = f"{cron}/{dpkg}/{kernel}/{named}/{pam_unix}/{sshd}/{sudo}/{disk_space}"

template_wiki["ns2"]  = f"{cron}/{dpkg}/{kernel}/{named}/{pam_unix}/{sshd}/{sudo}/{disk_space}"

template_wiki["nagios"]  = f"{cron}/{dpkg}/{httpd}/{named}/{kernel}/{pam_unix}/{sshd}/{connections}/{sudo}/{disk_space}"

template_wiki["bastion"]  = f"{cron}/{fail2ban}/{httpd}/{pam_unix}/{rsyslogd}/{sshd}/{connections}/{sudo}/{disk_space}/{kernel}"

template_wiki["erp"]  = f"{kernel_audit}/{cron}/{dpkg}/{httpd}/{kernel}/{named}/{pam_unix}/{rsyslogd}/{sshd}/{connections}/{sudo}/{disk_space}"

template_wiki["halnyftp"]  = f"{cron}/{kernel}/{pam_unix}/{connections}/{sshd}/{sudo}/{disk_space}"

template_wiki["halnydhcp"]  = f"{cron}/{kernel}/{pam_unix}/{connections}/{sshd}/{dhcp_logs}/{sudo}/{disk_space}"

#Aici vom tine conditiile pentru partea de cron pentru fiecare host in parte

conditii_cron = {}

conditii_cron["cacti2"] = {"user root" : {"sessionclean" : 48, "cron.daily" : 1, "cron.hourly" : 24, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 14" : 1}, "user www-data" : {"poller-error.log" : 288}}

conditii_cron["dhcp01"] = {"user root" : {"sessionclean" : 48, "cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 14" : 1, "dhcp-poll" : 1437}}

conditii_cron["pve4"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1, "vzdump 105" : 1,"vzdump 120" : 1}}

conditii_cron["nagios"] = {"user nagios1" : {"logrotate" : 1, "check_mk" : 1440, "update-dns" : 1, "diskspace" : 24}, "user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1, "sessionclean" : 48, "updatedns" : 24}}

conditii_cron["bastion"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1,}}

conditii_cron["fwbackbone"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1,}}

conditii_cron["speedtest"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1,}}

conditii_cron["ns"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1,}}

conditii_cron["dns1"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1}}

conditii_cron["ns2"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1,}}

conditii_cron["dns2"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1}}

conditii_cron["pve2"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1, "zfs-linux" : 1}}

conditii_cron["halnydhcp"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1}}

conditii_cron["pve7"] = {"user root" : {"zfs-linux" : 1, "cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1, "vzdump 107" : 1, "vzdump 125" : 1, "vzdump 126 - SAPTAMANAL ZIUA 3" : 1, "vzdump 134 - SAPTAMANAL ZIUA 6" : 1, "vzdump 240" : 1, "vzdump 244" : 1}}

conditii_cron["pve8"] = {"user root" : {"zfs-linux" : 1, "cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1, "vzdump 107" : 1, "vzdump 125" : 1, "vzdump 126 - SAPTAMANAL ZIUA 3" : 1, "vzdump 134 - SAPTAMANAL ZIUA 6" : 1, "vzdump 240" : 1, "vzdump 244" : 1}}

conditii_cron["halnyftp"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1,"cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1}}

conditii_cron["pve6"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1, "zfs-linux" : 1,"vzdump 107" : 1, "vzdump 125" : 1, "vzdump 126 - SAPTAMANAL ZIUA 3" : 1, "vzdump 134 - SAPTAMANAL ZIUA 6" : 1, "vzdump 240" : 1, "vzdump 244" : 1}}

conditii_cron["pve1"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1}}

conditii_cron["pve5"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1, "vzdump 105" : 1, "vzdump 120" : 1}}

conditii_cron["erp"] = {"user root" : {"cron.hourly" : 24, "cron.daily" : 1, "cron.weekly - SAPTAMANAL ZIUA 0" : 1, "cron.monthly - LUNAR PE 2" : 1, "certbot" : 2}, "user erp" : {"tunnel_voce_jcs_jo" : 289, "mail_scraper" : 1, "fusion_import" : 719, "duplicate_fibers" : 1, "duplicate_fibers" : 1, "import_from_fiber" : 1, "run_only_on_erp.sh" : 1439, "remove_old_notifications" : 1 }}

#Avem liste cu logurile care au filtre pentru anumite sectiuni, la care nu se raporteaza direct tot continutul

host_la_care_verificam_httpd = ["cacti2", "dhcp01", "speedtest"]

host_la_care_verificam_named = ["dns1", "dns2"]

host_la_care_verificam_smartd = ["pve7", "pve8"]

#Aici vom tine conditiile pentru MB transferred din httpd, in rest toate au aceleasi cerinte

conditii_httpd = {}

conditii_httpd["speedtest"] = {"mb_transferred_max" : 10000, "mb_transferred_min" : 500 }

conditii_httpd["dhcp01"] = {"mb_transferred_max" : 50}

conditii_httpd["cacti2"] = {"mb_transferred_max" : 5000}

#Tinem ipurile cunoscute intr-o lista care va fi folosita la verificarea celor aparute in loguri

ipuri_cunoscute = ["95.77.99.141", "82.208.169.2", "109.99.37.13", "startswith79.134.12"]

#Tinem in lista sectiunile acceptate din named

named_acceptate = ["Unexpected DNS RCODEs", "Incorrect response format"]

#Aici vom tine limitele de ocupare pentru partitiile la care nu se aplica regula cu 50%

conditii_speciale_disk_space = {}

conditii_speciale_disk_space["halnyftp"] = {"/" : 60} #Pentru halnyftp toate partitiile au limita acceptata de 60%

conditii_speciale_disk_space["halnydhcp"] = {"/" : 60} #Pentru halnydhcp toate partitiile au limita acceptata de 60%
   
conditii_speciale_disk_space["pve6"] = {"synbackup" : 70}

conditii_speciale_disk_space["pve7"] = {"synbackup" : 70}

conditii_speciale_disk_space["pve8"] = {"synbackup" : 70}


#Aici vom tine conditiile pentru backup successful pentru fiecare masina virtuala in parte

conditii_vm = {}

conditii_vm["105"] = {"durata" : "01:00:00", "dimensiune" : "30GB"}

conditii_vm["107"] = {"durata" : "02:00:00", "dimensiune" : "55GB"}

conditii_vm["120"] = {"durata" : "00:10:00", "dimensiune" : "1.5GB"}

conditii_vm["125"] = {"durata" : "00:30:00", "dimensiune" : "150GB"}

conditii_vm["126"] = {"durata" : "02:30:00", "dimensiune" : "25GB"}

conditii_vm["134"] = {"durata" : "01:00:00", "dimensiune" : "30GB"}

conditii_vm["240"] = {"durata" : "01:20:00", "dimensiune" : "135GB"}

conditii_vm["244"] = {"durata" : "00:05:00", "dimensiune" : "3GB"} 

#Tinem numele paginilor cu istoricul din wiki

nume_pagina_istoric = {}

for host in template_raport_monitorizare.split("/"):
    if "VM" in host:
        nume_pagina_istoric[host] = f"Istoric_backup_{host.lower()}_din_2021-04-02"
    else:
        nume_pagina_istoric[host] = f"Istoric_loguri_{host}_din_2021-04-02"
