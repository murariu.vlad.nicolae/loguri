import re
from templates import *
from creare_output import *


def verifica_backup_vm(log):
    rezultat_raport = ""
    #identificam randul care contine detaliile care ne intereseaza (numele masinii virtuale, statusul, durata si dimensiunea backup-ului)
    rand_detalii = re.search("VMID.*?\n(.*?)\n", log, flags = re.DOTALL).group(1)
    #identificam intai masina virtuala despre care este vorba si statusul backup-ului pentru a vedea daca putem continua verificarile, avem aici functia lower deoarece mi s-a parut ca in unele cazuri era "OK", nu "ok", la status si evitam eventualele probleme
    detalii = re.search("(\d+)\s+?(\w+)", rand_detalii.lower(), flags = re.DOTALL)
    nume_vm = detalii.group(1)
    status_backup = detalii.group(2)
    if status_backup == "ok":
        durata_si_dimensiune = re.search("\d+\s+.*?\s+\w+\s+(\d+:\d+:\d+)\s+(.*?)\s", rand_detalii, flags = re.DOTALL)
        durata = durata_si_dimensiune.group(1)
        dimensiune = durata_si_dimensiune.group(2)
        conditii_indeplinite = verifica_limite_backup(durata, dimensiune, nume_vm)
        if conditii_indeplinite["durata ok"] and conditii_indeplinite["dimensiune ok"]:
            rezultat_raport += f"Backup successful: {durata} {dimensiune}\n"
        else:
            erori = ""
            if not conditii_indeplinite["durata ok"]:
                erori += "DURATA BACKUP-ULUI A DEPASIT LIMITA IMPUSA! "
            if not conditii_indeplinite["dimensiune ok"]:
                erori += "DIMENSIUNEA BACKUP-ULUI A DEPASIT LIMITA IMPUSA!"
            rezultat_raport += f"Backup failed: {durata} {dimensiune} {erori}\n"
    else:
        rezultat_raport += "Backup failed!\n"
                
    rezultat_wiki = generare_data_log() + "\n" + rezultat_raport
    return {"host" : f"VM {nume_vm}", "raport wiki" : rezultat_wiki, "raport monitorizare" : rezultat_raport}


def verifica_limite_backup(durata, dimensiune, nume_vm):
    #dupa ce am verificat ca backup-ul a fost efectuat cu succes verificam si ca durata si dimensiunea sunt in limitele impuse, pe care le citim din templates
    durata_si_dimensiune_limita = conditii_vm[nume_vm]
    durata_limita = durata_si_dimensiune_limita["durata"]
    dimensiune_limita = durata_si_dimensiune_limita["dimensiune"]
    durata_ok = transforma_in_secunde(durata) < transforma_in_secunde(durata_limita)
    dimensiune_ok = formateaza_dimensiune(dimensiune) < formateaza_dimensiune(dimensiune_limita)
    
    return {"durata ok" : durata_ok, "dimensiune ok" : dimensiune_ok}


def transforma_in_secunde(durata):
    #transformam durata din formatul xx:xx:xx in secunde, pentru a o putea compara cu durata maxima in care trebuie sa se termine backup-ul
    unitati_timp = durata.split(":")
    return int(unitati_timp[0]) * 3600 + int(unitati_timp[1]) * 60 + int(unitati_timp[2])


def formateaza_dimensiune(dimensiune):
    #in cazul in care dimensiunea logului nu depaseste 1GB, aceasta se va exprima in MB si o vom transforma noi in GB pentru a o putea compara cu limitele impuse, exprimate in GB
    unitate_masura = re.search("\d+\.{0,1}\d{0,}([a-zA-Z]+)", dimensiune, flags = re.DOTALL).group(1)
    dimensiune_fara_unitate_masura = dimensiune.replace(unitate_masura, "")
    if unitate_masura.lower() == "mb":
        return float(dimensiune_fara_unitate_masura) / 1000
    else:
        return float(dimensiune_fara_unitate_masura)
