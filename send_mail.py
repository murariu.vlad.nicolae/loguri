from smtplib import SMTP_SSL, SMTP_SSL_PORT
from email.message import EmailMessage
from creare_output import *
from procesare_mailuri import *

username = 'incercari69@gmail.com'
password = 'gfydccgtbzkfvnyf'
host = 'imap.gmail.com'


def main():
    trimite_raport_monitorizare()


def trimite_raport_monitorizare():
    rezultate_procesare_mailuri = procesare_mailuri()
    au_aparut_probleme = rezultate_procesare_mailuri["au aparut probleme"]
    raport_monitorizare = rezultate_procesare_mailuri["raport monitorizare"]
    if au_aparut_probleme:
        print(text_alarma("Au aparut probleme in procesarea mailurilor, raportul nu a fost trimis, rezultatul il gasiti in raport_monitorizare.txt!"))
        scrie_in_fisier(raport_monitorizare)
    else:
        try:
            trimite_mail(raport_monitorizare, username)
        except:
            print(text_alarma("Nu s-a putut trimite mailul, rezultatul il gasiti in raport_monitorizare.txt!"))
        scrie_in_fisier(raport_monitorizare)
      

def scrie_in_fisier(raport_monitorizare):
    fisier_raport = open("raport_monitorizare.txt", "w")
    fisier_raport.write(raport_monitorizare)
    fisier_raport.close()
    

def trimite_mail(continut_mail, destinatar):
    #Compunem mesajul
    mail_raport = EmailMessage()
    mail_raport.add_header('Subject', f"Raport monitorizare {generare_data_log()}")
    mail_raport.set_content(continut_mail)
    #Ne conectam si trimitem mailul
    smtp_server = SMTP_SSL(host, port=SMTP_SSL_PORT)
    smtp_server.login(username, password)
    smtp_server.sendmail(username, destinatar, mail_raport.as_bytes())
    print(text_verde("Mailul a fost trimis (momentan catre Vlad), rezultatul il gasiti in raport_monitorizare.txt!"))
    #Ne deconectam de pe server
    smtp_server.quit()   


if __name__ == "__main__":
    main()

