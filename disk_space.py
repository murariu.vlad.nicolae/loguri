from selectare_continut import *
from creare_output import *
from selectare_continut import * 
from templates import *
import re


def verificare_disk_space(log, host):
    #se verifica daca spatiul ocupat este in limite si daca nu este se paseaza mai departe neregularitatile
    log_lines = separa_randurile(log)
    disk_space_ok = True
    rezultat = ""
    for rand in range(1, len(log_lines)):
        if not verificare_partitii(log_lines[rand], host):
            disk_space_ok = False
            rezultat += log_lines[rand] + "\n"
    if not disk_space_ok:
        rezultat = log_lines[0] + "\n" + rezultat
    rezultat = rezultat.strip()
    rezultat = creare_output_wiki(rezultat)
    return rezultat 


def separa_randurile(text):
    randuri_text = text.splitlines()
    randuri_text_fara_blanks = []
    for rand in randuri_text:
        if rand.strip() != "":
            randuri_text_fara_blanks.append(rand)
    return randuri_text_fara_blanks


def verificare_partitii(rand, host):
    #verificam daca hostul are conditii speciale sau se aplica cea generala cu 50%
    if host in conditii_speciale_disk_space:
        a_fost_identificata_partitia = False
        for partitie, limita in conditii_speciale_disk_space[host].items():
            if partitie in rand:
                partitie_ok = verificare_spatiu_ocupat(rand, limita)
                a_fost_identificata_partitia = True
        if not a_fost_identificata_partitia:
            partitie_ok = verificare_spatiu_ocupat(rand)
        return partitie_ok
    else:
        partitie_ok = verificare_spatiu_ocupat(rand)
        return partitie_ok


def verificare_spatiu_ocupat(rand, limita = 50):
    #se compara spatiul ocupat cu limita setata, default este 50, sunt cateva cazuri speciale in care este mai mare
    procent = re.search("(\d+)%", rand, flags = re.DOTALL).group(1)
    if int(procent) < limita:
        return True
    else:
        return False
    
