from templates import *
from datetime import date, timedelta
import random


def creare_output_wiki(rezultat_log):
    #se va lua rezultatul intepretarii fiecarei parti din log si se genereaza partea ce va fi adaugata in wiki
    rezultat_wiki = "OK"
    if rezultat_log != "":
        rezultat_wiki = rezultat_log
    return rezultat_wiki


def generare_data_mail():
    #se genereaza data folosita in imap search, in formatul corespunzator (ex: 28-Mar-2021)  
    log_date = date.today()
    log_date = log_date.strftime("%d-%b-%Y")
    return log_date


def generare_data_log():
    #se genereaza data logului, ziua precedenta interpretarii logului   
    log_date = date.today() - timedelta(1)
    log_date = log_date.strftime("%d.%m.%Y")
    return log_date


def template_raport_wiki(host):
    #se genereaza sablonul pt raportul din wiki bazat pe logul pe care il verificam acum, folosind partile asteptate din templates
    parti_asteptate = template_wiki[host].split("/")
    wiki = {}
    log_date = generare_data_log()
    wiki["data"] = "\t" + log_date
    for parte in parti_asteptate:
        wiki[parte] = "OK"
    #aici vom adauga orice parti neidentificate apar in log
    wiki["neidentificate"] = ""
    return wiki


def generare_raport_wiki(rezultate):
    #dupa ce s-au modificat valorile unde era cazul se formateaza raportul pentru a-l trece in wiki
    raport_wiki = ""
    for cheie, valoare in rezultate.items():
        if cheie == "data":
            raport_wiki += valoare + "\n\n"
        elif cheie == "neidentificate":
            if valoare != "":
                raport_wiki +="-NEIDENTIFICATE: " + cheie + ":\n\n" + valoare.strip() + "\n\n"
        elif valoare == "OK":
            raport_wiki +="-" + cheie + " : " + valoare.strip() + "\n\n"
        else:
            raport_wiki +="-" + cheie + ":\n\n" + valoare.strip() + "\n\n"
    return raport_wiki


def generare_raport_monitorizare(raport_wiki):
    #se genereaza raportul de monitorizare in care scriem doar ce este in plus, iar in cazul in care nu avem nimic de scris lasam un OK
    avem_de_raportat = False
    raport_monitorizare = ""
    for cheie, valoare in raport_wiki.items():
        if cheie == "neidentificate" and valoare != "":
            raport_monitorizare += "-" + cheie + ":\n\n" + valoare.strip() + "\n\n"
            avem_de_raportat = True
        elif valoare != "OK" and cheie not in ("data", "neidentificate"):
            raport_monitorizare +=  "-" + cheie + ":\n\n" + valoare.strip() + "\n\n"
            avem_de_raportat = True
    #daca nu avem nimic de raportat se va trece un singur OK
    if not avem_de_raportat:
        raport_monitorizare += " OK\n\n"
    return raport_monitorizare


def generare_template_raport_monitorizare():
    #se genereaza raportul de monitorizare in care vor fi completate rezultatele
    raport_monitorizare = {}
    for log in template_raport_monitorizare.split("/"):
        #backupul vm 126 vine doar miercurea
        if log == "VM 126" and date.today().weekday() != 2:
            raport_monitorizare[log] = "DOAR MIERCURI"
        #backupul vm 134 vine doar sambata
        elif log == "VM 134" and date.today().weekday() != 5:
            raport_monitorizare[log] = "DOAR SAMBATA"
        else:
            raport_monitorizare[log] = "NU A VENIT"
    return raport_monitorizare

    
def generare_mail_raport_monitorizare(raport_monitorizare):
    #se compune mailul ce va fi trimis
    continut_mail = ""
    increment = 1
    for log, rezultat in raport_monitorizare.items():
        continut_mail += f"\t{increment}. {log}:\n\n{rezultat.strip()}\n\n"
        increment += 1
    return continut_mail
    

def generare_pin():
    return f"{random.randint(0,9)}{random.randint(0,9)}{random.randint(0,9)}{random.randint(0,9)}" 
    

def text_alarma(text):
    return f"\033[92m{text}\033[0m \x1b[6;69;41m ERROR! \x1b[0m"


def text_verde(text):
    return f"\033[99m{text}\033[0m"
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
