import imaplib
import email
from creare_output import *
from interpretare_log import *
from loguri_masini_virtuale import *
import requests
from templates import *
import random

#avem datele contului de mail
username = 'monitor@jcs.jo'
password = 'tpS#Np^3n3vR6$v5'
host = 'mail.jcs.jo'
 

def procesare_mailuri():
    #se genereaza raportul in care se vor completa rezultatele
    raport = generare_template_raport_monitorizare()
    #ne logam pe mail si alegem folderul din care selectam doar mailurile primite azi
    server = logare_mail(username, password)
    lista_mailuri = selectare_mailuri_de_azi(server)
    lista_mailuri = lista_mailuri[0].split()
    i = 1
    au_aparut_probleme = False
    PIN = generare_pin()
    pozitie_PIN = random.randint(3, len(lista_mailuri)-3)
    nr_mail = 1
    for id_mail in lista_mailuri:
        if nr_mail == pozitie_PIN:
            print(f"PIN: {PIN}")
        _, msg = server.fetch(id_mail, '(RFC822)')
        mail = email.message_from_bytes(msg[0][1]) 
        #identificam mailurile in functie de subiect si le impartim in loguri, backupuri si cele care nu se fac
        subiect_mail = mail["subject"]
        tip_mail = identifica_mail(subiect_mail)
        if tip_mail in ["backup", "log"]:
            #interpretam doar mailurile care ne intereseaza, folosind functia corespunzatoare
            continut_mail = extrage_continut_mail(mail)
            try:
                raport = proceseaza_mail(tip_mail, continut_mail, raport, subiect_mail)
            except:
                print(text_alarma(f"Au aparut probleme in interpretarea mail-ului {subiect_mail}!"))
                au_aparut_probleme = True
        nr_mail += 1      
    if verifica_status_raport(raport):
        au_aparut_probleme = True
    PIN_introdus = ""
    while(PIN_introdus != PIN):
        print("Introduceti PIN-ul pentru a confirma ca ati citit tot si a primi rezultatul:")
        PIN_introdus = input()
    
    return {"raport monitorizare" : generare_mail_raport_monitorizare(raport), "au aparut probleme" : au_aparut_probleme}
   

def logare_mail(username, password):
    #ne logam pe mail
    imap_server = imaplib.IMAP4_SSL(host)
    imap_server.login(username, password)
    #selectam folderul in care avem treaba
    imap_server.select()  
    return imap_server


def selectare_mailuri_de_azi(imap_server):
    #cautam doar mailurile din ziua curenta
    search_criteria = f"ON {generare_data_mail()}"
    charset = "UTF-8"
    respose_code, message_numbers_raw = imap_server.search(charset, search_criteria)
    return message_numbers_raw


def identifica_mail(subiect_mail):
    #se stabileste daca este vorba despre un backup sau un log care va fi interpretat
    if "vzdump" in subiect_mail and re.search("vzdump backup status \((.*)\)", subiect_mail, flags = re.DOTALL).group(1) in backups:
        return "backup"
    elif "Logwatch" in subiect_mail and re.search("Logwatch for (.*) \(Linux\)", subiect_mail, flags = re.DOTALL).group(1) in loguri_lungi:
        return "log"
    else:
        return "nu se face"
    

def extrage_continut_mail(mail):
    #daca mesajul este multipart se selecteaza partea text/plain pentru a fi procesata si se scot \r-urile despre care nu stiu de unde apar
    if mail.is_multipart():
        for part in mail.walk():
            if part.get_content_type() == "text/plain":
                continut_mail = part.as_string()
    else: 
        continut_mail = mail.get_payload()
    return continut_mail.replace("\r", "")


def proceseaza_mail(tip_mail, continut_mail, raport, subiect_mail):
    rezultate_interpretare_mail = interpreteaza_mail(tip_mail, continut_mail)
    host = rezultate_interpretare_mail["host"]
    raport_monitorizare = rezultate_interpretare_mail["raport monitorizare"]
    raport_wiki = rezultate_interpretare_mail["raport wiki"]
    #completam raportul de monitorizare
    raport[host] = raport_monitorizare
    #completam istoricul din wiki
    raport_wiki = formateaza_text_pentru_wiki(raport_wiki)
    try:
        completeaza_raport_wiki(host, raport_wiki)
    except:
        print(text_alarma(f"Nu s-a putut completa istoricul in wiki pentru {host}!"))
    print(text_verde(f"Mailul {subiect_mail} a fost interpretat!"))
    return raport


def interpreteaza_mail(tip_mail, continut_mail):
    #se alege metoda de verificare a mailului
    if tip_mail == "backup":
        return verifica_backup_vm(continut_mail)
    else:
        return interpretare_log(continut_mail)


def completeaza_raport_wiki(host, rezultat):
    if not verifica_daca_este_deja_trecut_rezultatul(host):
        adauga_rezultat_in_wiki(host, rezultat)
        print(text_verde(f"A fost completat istoricul in wiki pentru {host}!"))
    else:
        print(text_verde(f"Era deja completat istoricul in wiki pentru {host}!"))


def verifica_daca_este_deja_trecut_rezultatul(host):
    sesiune = requests.Session()
        
    url_wiki = "http://10.1.1.11/mediawiki/api.php"
      
    parametri_request_content = {
    	"action": "parse",
    	"format": "json",
    	"page": f"{nume_pagina_istoric[host]}",
    	"formatversion": "2",
    }
     
    request_continut = sesiune.get(url=url_wiki, params=parametri_request_content)
    raspuns_request_continut = request_continut.json()
    
    return generare_data_log() in raspuns_request_continut["parse"]["text"]


def adauga_rezultat_in_wiki(host, rezultat):
    #se completeaza in istoricul din wiki rezultatul
    sesiune = requests.Session()
    
    url_wiki = "http://10.1.1.11/mediawiki/api.php"
  
    parametri_request_token = {
        "action": "query",
        "meta": "tokens",
        "format": "json"
    }
    request_token = sesiune.get(url=url_wiki, params=parametri_request_token)
    raspuns_request_token = request_token.json()
    
    csrf_token = raspuns_request_token['query']['tokens']['csrftoken']
    
    parametri_request_edit = {
        "action": "edit",
        "title": f"{nume_pagina_istoric[host]}",
        "token": csrf_token,
        "format": "json",
        "prependtext" : f"{rezultat}\n\n"
    }
    
    request_edit = sesiune.post(url_wiki, data=parametri_request_edit)
    

def formateaza_text_pentru_wiki(text):
    text = re.sub("\n", "\n\n", text)
    text = re.sub("\n{2,}", "\n\n", text)
    return text


def verifica_status_raport(raport):
    raport_incomplet = False
    for host, rezultat_host in raport.items():
        if rezultat_host == "NU A VENIT":
            print(text_alarma(f"Mailul de la {host} nu a venit!"))
            raport_incomplet = True
    return raport_incomplet


