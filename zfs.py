from selectare_continut import *
from creare_output import *
from templates import *
import re


def verificare_zfs(log):
    rezultat = ""
    #izolam si verificam partea cu totalurile
    partea_cu_totalurile = re.search("(Total.*?)\n\n", log, flags = re.DOTALL).group(1)
    rezultat += verifica_section_zfs(partea_cu_totalurile, zfs_totaluri_asteptate)
    log_ramas = log.replace(partea_cu_totalurile, "")
    #stergem partea de zfs pool summary, deoarece aceasta nu este verificata in niciun fel
    log_ramas = section_zfs(log_ramas, zfs_pool_summary)["log ramas"]
    #izolam si verificam partea pool status
    pool_status = section_zfs(log_ramas, zfs_pool_status)
    rezultat += verifica_section_zfs(pool_status["continut sectiune"], zfs_pool_status_conditii)
    log_ramas = pool_status["log ramas"]
    #pentru eventualitatea in care a aparut ceva in plus in log il adaugam la rezultat
    rezultat += log_ramas.strip()
    return creare_output_wiki(rezultat)


def verifica_section_zfs(log, dictionar_conditii):
    #se verifica sectiunea din zfs data folosind conditiile trecute in dictionarul creat pentru acea sectiune
    toate_ok = True
    for ce_cautam, conditii in dictionar_conditii.items():
        for regex, valoare in conditii.items():
            if re.search(regex, log, flags = re.DOTALL).group(1).lower() != valoare:
                return log
    return ""


def section_zfs(log, section_name):
    #selectam din partea de ZFS Report partea ceruta in section_name (zfs pool status sau zfs pool summary) si returnam partea izolata si logul din care s-a sters acea parte
    partea_ceruta = re.search(f"(-+ {section_name} -+.*?-{{5,}})", log, flags = re.DOTALL).group(1)
    log_fara_partea_ceruta = log.replace(partea_ceruta, "")
    #identificam si stergem liniile care separa subsectiunea in log (-----nume----- si -----------)
    separatoare_continut = re.findall(f"-+ {section_name} -+|-{{5,}}", partea_ceruta, flags = re.DOTALL) 
    for separator in separatoare_continut:
        partea_ceruta = partea_ceruta.replace(separator, "") 
    return {"continut sectiune" : partea_ceruta, "log ramas" : log_fara_partea_ceruta}
    
