import re

def identificare_host(log):
    #identificam hostul al carui log va fi procesat pentru a folosii conditiile corespunzatoare
    return re.search("Logfiles for Host: (.*?)\n", log, flags = re.DOTALL).group(1)


def formatare_log(log):
    #stergem spatiile goale de la inceputul si finalul randurilor pentru a fi mai usor sa folosim expresiile regulate    
    #stergem spatiile goale de la inceputul randurilor
    log_fara_spatii = re.sub(r"^([ \t]*)", "", log, 0, re.MULTILINE)
    #stergem spatiile goale de la finalul randurilor
    log_fara_spatii = re.sub(r"([ \t]*)$", "", log_fara_spatii, 0, re.MULTILINE)
    #selectam din mail doar partea care ne intereseaza, fara detaliile de la inceput  
    log_continut = re.search("^\s*###.*?$.*?### *$(.*?)###", log_fara_spatii, flags = re.DOTALL | re.MULTILINE).group(1)
    return log_continut


def section(log, section_name):
    #selectam din log partea specificata in section_name    
    continut = re.search(f'(-+ {section_name} Begin -+\s+.*\s+-+ {section_name} End -+)', log, flags = re.DOTALL).group(1)
    log = log.replace(continut, "")
    #se identifica liniile de inceput si de final care doar delimiteaza sectiunea
    separatoare_continut = re.findall(f'(-+ {section_name} Begin -+|-+ {section_name} End -+)', continut)
    for separator in separatoare_continut:
        continut = continut.replace(separator, "")  
    return {"log ramas" : log, "continut sectiune" : continut}

