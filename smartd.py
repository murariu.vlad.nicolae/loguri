from selectare_continut import *
from creare_output import *
from templates import *
import re


def verificare_smartd(log, host):
    if host in host_la_care_verificam_smartd:
        rezultat = ""
        grupuri_de_verificat = impartire_pe_grupuri(log)
        grupuri_cu_erori = verificare_grupuri(grupuri_de_verificat) 
        for grup in grupuri_cu_erori:
            rezultat += grup + "\n" 
        return creare_output_wiki(rezultat)
    else:
        return log
    

def impartire_pe_grupuri(log):
    #impartim partea de smartd in grupuri pentru a verifica separat daca au erori
    return re.findall("(/.*?)\n\n", log, flags = re.DOTALL)
     

def verificare_grupuri(grupuri):
    #se cauta in fiecare grup erori si se dau mai departe doar cele care contin
    grupuri_cu_erori = []
    for grup in grupuri:
        if "Read_Soft_Error_Rate" in grup:
            grupuri_cu_erori.append(grup)
    return grupuri_cu_erori
