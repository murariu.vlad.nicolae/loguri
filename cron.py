from selectare_continut import *
from creare_output import *
from templates import *
import re
from datetime import date


def verificare_cron(continut_cron, host):
    rezultat = ""
    total_useri = {}
    useri = impartire_pe_useri(continut_cron)
    total_useri = total_pe_useri(useri)
    #pentru fiecare user care stim ca trebuie sa apara verificam daca a aparut ce asteptam si cand asteptam (lunar, saptamanal, zilnic)
    for user_asteptat, conditii_de_indeplinit in conditii_cron[host].items():
        for ce_cautam, total_asteptat in conditii_de_indeplinit.items():
            if " - LUNAR PE" in ce_cautam:
                ziua_asteptata = ce_cautam.split(" - LUNAR PE ")[1] 
                if int(ziua_asteptata) == date.today().day:
                    ce_cautam = ce_cautam.replace(f" - LUNAR PE {ziua_asteptata}", "")
                    useri[user_asteptat] = verifica_sectiune(useri[user_asteptat], ce_cautam, total_asteptat)
            elif " - SAPTAMANAL ZIUA " in ce_cautam:
                ziua_asteptata = ce_cautam.split(" - SAPTAMANAL ZIUA ")[1]   
                if int(ziua_asteptata) == date.today().weekday():
                    ce_cautam = ce_cautam.replace(f" - SAPTAMANAL ZIUA {ziua_asteptata}", "")
                    useri[user_asteptat] = verifica_sectiune(useri[user_asteptat], ce_cautam, total_asteptat) 
            else:
                useri[user_asteptat] = verifica_sectiune(useri[user_asteptat], ce_cautam, total_asteptat) 
            
    rezultat = formeaza_rezultat(useri)
    
    return {"rezultat" : creare_output_wiki(rezultat.strip()), "total useri" : total_useri}


def impartire_pe_useri(continut_cron):
    #se imparte continutul din cron in functie de useri
    useri = {}
    cron_impartit = continut_cron.strip().split("User ")
    for parte in cron_impartit:
        if "Commands Run:" in parte:
            pass
        else:
            nume_user = parte.split(":", 1)[0]
            continut_user = parte.split(":", 1)[1].strip()
            useri[f"user {nume_user}"] = continut_user
    return useri


def total_pe_useri(useri):
    #se calculeaza totalul pentru fiecare user in parte
    useri_si_total = {}
    for user, continut in useri.items():
        total = 0
        times = re.findall("(\d+) Time", continut, flags = re.DOTALL)
        for time in times:
            total += int(time)
        useri_si_total[user] = total
    return useri_si_total
        

def verifica_sectiune(section, ce_cautam, total_asteptat):
    #se imparte continutul in linii separate pentru a cauta in fiecare linie si a se sterge doar cea in care am gasit ce cautam
    section_lines = section.splitlines()
    for line in section_lines:
        if ce_cautam in line:
            total_aparut = re.search(f"{ce_cautam}.*?(\d+) Time", line, flags = re.DOTALL).group(1)
            if int(total_aparut) == total_asteptat:
                section_lines.remove(line)
    return "\n".join(section_lines)


def formeaza_rezultat(useri):
    #se verifica daca a mai ramas ceva pentru fiecare user
    rezultat = ""
    for user, continut in useri.items():
        if continut.strip() != "":
            rezultat += f"\t{user}:\n{continut}\n"   
    return rezultat
  

