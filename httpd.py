import re
from creare_output import *
from selectare_continut import *
from templates import *


def verificare_httpd(continut_httpd, host):   
    #verificam daca ip-urile sunt cunoscute, cati mb au fost transferati, attempts to use known hacks, requests with error response codes  
    if host in host_la_care_verificam_httpd:
        rezultat = ""
        
        limite_transferred = identificare_limite_transferred(host)
        mb_transferred_max = limite_transferred["mb transferred max"]
        mb_transferred_min = limite_transferred["mb transferred min"]
        mb_transferred = verificare_mb_transferred(continut_httpd, mb_transferred_max, mb_transferred_min)
        continut_httpd = mb_transferred["httpd ramas"]
        rezultat += mb_transferred["rezultat"]
       
        hacks = verificare_hacks(continut_httpd)
        continut_httpd = hacks["httpd ramas"]
        rezultat += hacks["rezultat"]
    
        probed = verificare_probed(continut_httpd)
        continut_httpd = probed["httpd ramas"]
        rezultat += probed["rezultat"]    
        
        requests = verificare_requests(continut_httpd)
        continut_httpd = requests["httpd ramas"]
        rezultat += requests["rezultat"] 
        
        #verificam daca a ramas ceva
        continut_httpd = continut_httpd.strip()
        rezultat += continut_httpd
        
        #se formeaza rezultatul final ce va fi trecut in wiki si in raportul de monitorizare
        return creare_output_wiki(rezultat.strip())
    else:
        return creare_output_wiki(continut_httpd)


def identificare_limite_transferred(host):
    #luam limitele mb transferred cu care se va lucra din templates
    mb_transferred_max, mb_transferred_min = 0, 0
    conditii_host = conditii_httpd[host]
    if "mb transferred max" in conditii_host:
        mb_transferred_max = conditii_host["mb transferred max"]
    if "mb transferred min" in conditii_host:
        mb_transferred_min = conditii_host["mb transferred min"] 
    return {"mb transferred max" : mb_transferred_max, "mb transferred min" : mb_transferred_min}


def verificare_mb_transferred(continut_httpd, mb_transferred_max = 0, mb_transferred_min = 0):
    #verificam daca mb transferred sunt in limitele setate, daca acestea sunt setate
    rezultat = ""
    if "MB transferred" in continut_httpd:
        transferred_max_ok, transferred_min_ok = True, True
        transferred = re.search("(\d+\.\d+) MB transferred", continut_httpd, flags = re.DOTALL).group(1)
        if mb_transferred_max != 0:
            transferred_max_ok = False
            if float(transferred) < mb_transferred_max:
                transferred_max_ok = True  
        if mb_transferred_min != 0:
            transferred_min_ok = True
            if float(transferred) < mb_transferred_min:
                transferred_min_ok = True  
        transferred = re.search("(\d+.\d+ MB transferred .*?\n\n)", continut_httpd, flags = re.DOTALL).group(1)  
        if not transferred_max_ok or not transferred_min_ok:
            rezultat += transferred + " MB TRANSFERRED NU SUNT IN LIMITE!" + "\n\n"
        continut_httpd = continut_httpd.replace(transferred, "")
    return {"httpd ramas" : continut_httpd, "rezultat" : rezultat}


def verificare_hacks(continut_httpd):
    #logam attempts to use known hacks
    rezultat = ""
    if "Attempts to use known hacks" in continut_httpd:
        hacks = re.search("(Attempts to use .*?)\n{3}", continut_httpd, flags = re.DOTALL).group(1)
        rezultat += hacks + "\n\n"
        continut_httpd = continut_httpd.replace(hacks, "")  
    return {"httpd ramas" : continut_httpd, "rezultat" : rezultat}


def verificare_probed(continut_httpd):   
    #verificam daca ip-urile din sectiunea probed the server sunt cunoscute si afisam eventualele ip-uri necunoscute
    rezultat = ""
    ipuri_necunoscute = ""
    probed_ok = True
    if "probed the server" in continut_httpd:
        probed = re.search("(A total of \d+ sites probed the server\n.*?)\n\n", continut_httpd, flags = re.DOTALL).group(1)
        ipuri = re.findall("(\d+\.\d+\.\d+\.\d+)", probed, flags = re.DOTALL)
        for ip in ipuri:
            if not verificare_ip(ip):
                probed_ok = False
                ipuri_necunoscute += ip + " "
        if not probed_ok:
            rezultat += "IP-uri necunoscute care au accesat serverul: " + ipuri_necunoscute + "\n\n"
        continut_httpd = continut_httpd.replace(probed, "")
    return {"httpd ramas" : continut_httpd, "rezultat" : rezultat}


def verificare_ip(ip):
    #verificam daca ip-ul este cunoscut
    ip_ok = False
    for ip_cunoscut in ipuri_cunoscute:
        if "startswith" in ip_cunoscut:
            if ip.startswith(ip_cunoscut.replace("startswith", "")):
                ip_ok = True
        else:
            if ip == ip_cunoscut:
                ip_ok = True
    return ip_ok


def verificare_requests(continut_httpd):
    #logam requests with error response codes
    rezultat = ""
    if "Requests with error response codes" in continut_httpd:
        requests = re.search("(Requests with error response codes.*?Time\(s\)\n\n)", continut_httpd, flags = re.DOTALL).group(1)
        rezultat += requests + "\n\n"
        continut_httpd = continut_httpd.replace(requests, "")  
    return {"httpd ramas" : continut_httpd, "rezultat" : rezultat}
