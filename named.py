import re
from templates import *
from selectare_continut import *
from creare_output import *


def verificare_named(continut_named, host):
    #se verifica daca hostul este printre cei pentru care si interpretam datele si se sterg cele acceptate
    if host in host_la_care_verificam_named:
        for parte in named_acceptate:
            if parte in continut_named:
                de_sters = re.search(f"({parte}:\n.*?)\n\n", continut_named, flags = re.DOTALL).group(1)
                continut_named = continut_named.replace(de_sters, "")
    return creare_output_wiki(continut_named.strip())
                









